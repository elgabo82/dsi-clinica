package capa.logica;

import java.util.Date;

/**
 * DSI - POO - 2023-S1 24.06.2023 22.10
 * Capa Lógica - Clase Persona
 * @author Gabriel Eduardo Morejón López
 */
public class Persona {
    private String cedula;
    private String nombres;
    private String apellidos;
    private String telefono;
    private String direccion;
    private String correo;
    private Date fechaNacimiento;
    private boolean menorEdad;
    private boolean adultoMayor;
    
    // Constructores

    public Persona() {
    }

    public Persona(String cedula, String nombres, String apellidos, String telefono, String direccion, String correo, Date fechaNacimiento, boolean menorEdad, boolean adultoMayor) {
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono = telefono;
        this.direccion = direccion;
        this.correo = correo;
        this.fechaNacimiento = fechaNacimiento;
        this.menorEdad = menorEdad;
        this.adultoMayor = adultoMayor;
    }
    
    // Getters y Setters

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public boolean isMenorEdad() {
        return menorEdad;
    }

    public void setMenorEdad(boolean menorEdad) {
        this.menorEdad = menorEdad;
    }

    public boolean isAdultoMayor() {
        return adultoMayor;
    }

    public void setAdultoMayor(boolean adultoMayor) {
        this.adultoMayor = adultoMayor;
    }
    
    
}
