package capa.logica;

import java.util.Date;
import java.util.List;

/**
 * DSI - POO - 2023-S1 24.06.2023 22.11
 * Capa Lógica - Clase Médico
 * @author Gabriel Eduardo Morejón López
 */
public class Medico extends Persona {
    private int idMedico;
    private String especialidad;
    private List<Turnos> listaTurnos;
    private Usuario usuario;
    private Horarios horario;

    // Constructores
    public Medico() {
    }

    public Medico(int idMedico, String especialidad, List<Turnos> listaTurnos, 
            Usuario usuario, Horarios horario, String cedula, String nombres, 
            String apellidos, String telefono, String direccion, String correo, 
            Date fechaNacimiento, boolean menorEdad, boolean adultoMayor) {
        super(cedula, nombres, apellidos, telefono, direccion, correo, fechaNacimiento, menorEdad, adultoMayor);
        this.idMedico = idMedico;
        this.especialidad = especialidad;
        this.listaTurnos = listaTurnos;
        this.usuario = usuario;
        this.horario = horario;
    }

    // Getters y Setters

    public int getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(int idMedico) {
        this.idMedico = idMedico;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public List<Turnos> getListaTurnos() {
        return listaTurnos;
    }

    public void setListaTurnos(List<Turnos> listaTurnos) {
        this.listaTurnos = listaTurnos;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Horarios getHorario() {
        return horario;
    }

    public void setHorario(Horarios horario) {
        this.horario = horario;
    }
    
    
    
    
}
