package capa.logica;

import java.util.Date;
import java.util.List;

/**
 * DSI - POO - 2023-S1 24.06.2023 22.11
 * Capa Lógica - Clase Usuario
 * @author Gabriel Eduardo Morejón López
 */
public class Usuario extends Persona {
    private int idPerfil;
    private String nombreUsuario;
    private String claveUsuario;    
    private List<Perfil> perfil;
    
    // Constructores
    public Usuario() {
    }    

    public Usuario(int idPerfil, String nombreUsuario, String claveUsuario, List<Perfil> perfil, String cedula, String nombres, String apellidos, String telefono, String direccion, String correo, Date fechaNacimiento, boolean menorEdad, boolean adultoMayor) {
        super(cedula, nombres, apellidos, telefono, direccion, correo, fechaNacimiento, menorEdad, adultoMayor);
        this.idPerfil = idPerfil;
        this.nombreUsuario = nombreUsuario;
        this.claveUsuario = claveUsuario;
        this.perfil = perfil;
    }

    // Getters y Setters

    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getClaveUsuario() {
        return claveUsuario;
    }

    public void setClaveUsuario(String claveUsuario) {
        this.claveUsuario = claveUsuario;
    }

    public List<Perfil> getPerfil() {
        return perfil;
    }

    public void setPerfil(List<Perfil> perfil) {
        this.perfil = perfil;
    }
    
    
    
    
    
}
