package capa.logica;

/**
 * DSI - POO - 2023-S1 24.06.2023 22.11
 * Capa Lógica - Clase Perfil
 * @author Gabriel Eduardo Morejón López
 */
public class Perfil {
    private int idPerfil;
    private String tipoPerfil;
    private String observaciones;

    // Constructores
    public Perfil() {
    }

    public Perfil(int idPerfil, String tipoPerfil, String observaciones) {
        this.idPerfil = idPerfil;
        this.tipoPerfil = tipoPerfil;
        this.observaciones = observaciones;
    }

    // Getters y Setters
    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getTipoPerfil() {
        return tipoPerfil;
    }

    public void setTipoPerfil(String tipoPerfil) {
        this.tipoPerfil = tipoPerfil;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    
    
    
}
