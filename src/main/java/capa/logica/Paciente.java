package capa.logica;

import java.util.Date;
import java.util.List;

/**
 * DSI - POO - 2023-S1 24.06.2023 22.11
 * Capa Lógica - Clase Paciente
 * @author Gabriel Eduardo Morejón López
 */
public class Paciente extends Persona {
    private int nroHClinica;
    private String tipoSeguro;
    private String tipoSangre;
    // Colección - Relación 1 a n
    private List<Turnos> listaTurnos;
    private Usuario usuario;

    // Constructores
    public Paciente() {
    }

    public Paciente(int nroHClinica, String tipoSeguro, String tipoSangre, List<Turnos> listaTurnos, Usuario usuario, String cedula, String nombres, String apellidos, String telefono, String direccion, String correo, Date fechaNacimiento, boolean menorEdad, boolean adultoMayor) {
        super(cedula, nombres, apellidos, telefono, direccion, correo, fechaNacimiento, menorEdad, adultoMayor);
        this.nroHClinica = nroHClinica;
        this.tipoSeguro = tipoSeguro;
        this.tipoSangre = tipoSangre;
        this.listaTurnos = listaTurnos;
        this.usuario = usuario;
    }
    
    // Getters y Setters    
    public int getNroHClinica() {
        return nroHClinica;
    }

    public void setNroHClinica(int nroHClinica) {
        this.nroHClinica = nroHClinica;
    }

    public String getTipoSeguro() {
        return tipoSeguro;
    }

    public void setTipoSeguro(String tipoSeguro) {
        this.tipoSeguro = tipoSeguro;
    }

    public String getTipoSangre() {
        return tipoSangre;
    }

    public void setTipoSangre(String tipoSangre) {
        this.tipoSangre = tipoSangre;
    }

    public List<Turnos> getListaTurnos() {
        return listaTurnos;
    }

    public void setListaTurnos(List<Turnos> listaTurnos) {
        this.listaTurnos = listaTurnos;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
           
    
}
