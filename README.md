# Aplicación básica para registro de Pacientes
Aplicación de práctica del modelo de 3 capas, Web para aplicar conceptos de POO y de Desarrollo de Sistemas Informáticos

# Capa de Presentación o Interfaz
# Capa Lógica
# Capa de Persistencia

# Conceptos de Herencia, Polimorfismo, Encapsulación, JPA, Bases de Datos, etc.
# Unidades de Persistencia

# Lenguaje
## Java

```
System.out.println("Ejercicio de Programación Orientada a Objetos y de Desarrollo de Sistemas.");
```

# Autor
## Gabriel Eduardo Morejón López
##@elgabo82
